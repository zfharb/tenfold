package tenfold;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginTest {
	
	private static WebDriver driver;
	private static WebDriverWait wait;
	
	@Before
	public void setUp() throws InterruptedException {
		System.out.println("*******************");
		System.out.println("launching chrome browser");
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		System.out.println("*******************");

		driver = new ChromeDriver();
		wait=new WebDriverWait(driver,5);

		driver.get("https://qa-engineer-test.firebaseapp.com");

	}
	
		
	@Test
	/*
	 * positive happy path: login with valid user name and password
	 */
	public void testLoginWithValidCredentials() throws InterruptedException {
		System.out.println("*******************");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='username']")));		

		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='login-button']")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		
	}
	
	@Test
	/*
	 * negative test: Login with invalid user name
	 */
	public void testLoginWithInValidUserName() throws InterruptedException {
		System.out.println("*******************");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='username']")));		

		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("tenfold@tenfold.xxx");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='login-button']")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/alert/div")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/alert/div")).getText().contentEquals("Unknow Error!"));
		
	}
	
	@Test
	/*
	 * negative test: login with invalid password shorter than 6 chars
	 */
	public void testLoginWithInValidPasswordShorterThansixChar() throws InterruptedException {
		System.out.println("*******************");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='username']")));		

		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123");
		driver.findElement(By.xpath("//*[@id='login-button']")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/alert/div")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/alert/div")).getText().contentEquals("Unknow Error!"));
		
	}
	
	@Test
	/*
	 * negative test: login with invalid password longer than six chars
	 */
	public void testLoginWithInValidPasswordLongerThanSixChar() throws InterruptedException {
		System.out.println("*******************");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='username']")));		

		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123");
		driver.findElement(By.xpath("//*[@id='login-button']")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/alert/div")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/alert/div")).getText().contentEquals("Unknow Error!"));
		
	}
	
	@After
	public void tearDown() {
		
		driver.close();
		driver.quit();
	}

}
