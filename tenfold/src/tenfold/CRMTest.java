package tenfold;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CRMTest {
	
	private static WebDriver driver;
	private static WebDriverWait wait;
	
	@Before
	public void setUp() throws InterruptedException {
		System.out.println("*******************");
		System.out.println("launching chrome browser");
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		System.out.println("*******************");

		driver = new ChromeDriver();
		wait=new WebDriverWait(driver,5);

		driver.get("https://qa-engineer-test.firebaseapp.com");
		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='login-button']")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		
	}
	
		
	@Test
	public void testLogout() throws InterruptedException {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		driver.findElement(By.xpath("/html/body/app-root/div[1]/div/ul/li/a")).click();
		ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='username']"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-login/div/h2")).getText().contentEquals("Login"));

	}

	@Test
	public void testCRMSalesForce() throws InterruptedException {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		driver.findElement(By.xpath("//*[@id='crm']/div/div/div[1]")).click();
		driver.findElement(By.xpath("//*[@id='crm']/select-dropdown/div/div[2]/ul/li[1]")).click();


		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='save-button']")).click();
		
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label")).getText().contentEquals("Phone System"));

	}
	
	
	@Test
	public void testCRMZoho() throws InterruptedException {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		driver.findElement(By.xpath("//*[@id='crm']/div/div/div[1]")).click();
		driver.findElement(By.xpath("//*[@id='crm']/select-dropdown/div/div[2]/ul/li[2]")).click();
		
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='save-button']")).click();
		
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label")).getText().contentEquals("Phone System"));

	}
	
	@Test
	public void testCRMSugar() throws InterruptedException {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		driver.findElement(By.xpath("//*[@id='crm']/div/div/div[1]")).click();
		driver.findElement(By.xpath("//*[@id='crm']/select-dropdown/div/div[2]/ul/li[3]")).click();
		
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='save-button']")).click();
		
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label")).getText().contentEquals("Phone System"));

	}
	
	@Test
	public void testCRMOracle() throws InterruptedException {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		driver.findElement(By.xpath("//*[@id='crm']/div/div/div[1]")).click();
		driver.findElement(By.xpath("//*[@id='crm']/select-dropdown/div/div[2]/ul/li[4]")).click();
		
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='save-button']")).click();
		
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label")).getText().contentEquals("Phone System"));

	}
	
	@Test
	public void testCRMDynamics() throws InterruptedException {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		driver.findElement(By.xpath("//*[@id='crm']/div/div/div[1]")).click();
		driver.findElement(By.xpath("//*[@id='crm']/select-dropdown/div/div[2]/ul/li[5]")).click();
		
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='save-button']")).click();
		
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label")).getText().contentEquals("Phone System"));

	}

	@After
	public void tearDown() {
		
		driver.close();
		driver.quit();
	}

}
