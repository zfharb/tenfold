package tenfold;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReviewTest {
	
	private static WebDriver driver;
	private static WebDriverWait wait;
	
	@Before
	public void setUp() throws InterruptedException {
		System.out.println("*******************");
		System.out.println("launching chrome browser");
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		System.out.println("*******************");

		driver = new ChromeDriver();
		wait=new WebDriverWait(driver,5);

		driver.get("https://qa-engineer-test.firebaseapp.com");
		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='login-button']")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")));		
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-crm/form/div[1]/div/div/label")).getText().contentEquals("CRM"));
		driver.findElement(By.xpath("//*[@id='crm']/div/div/div[1]")).click();
		driver.findElement(By.xpath("//*[@id='crm']/select-dropdown/div/div[2]/ul/li[1]")).click();


		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='save-button']")).click();
		
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label")).getText().contentEquals("Phone System"));

		driver.findElement(By.xpath("//*[@id='phonesystem']/div/div/div[2]")).click();
		driver.findElement(By.xpath("//*[@id='phonesystem']/select-dropdown/div/div[2]/ul/li[1]")).click();

		driver.findElement(By.xpath("//*[@id='ip']")).sendKeys("127.0.0.0");

		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='finish-button']")).click();
		
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-review/h1"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/h1")).getText().contains("Review Page"));

	}
	
		
	@Test
	public void testReviewPage() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id='back-button']")).click();
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-phonesystem/form/div[1]/div/div/label")).getText().contentEquals("Phone System"));

		driver.findElement(By.xpath("//*[@id='phonesystem']/div/div/div[2]")).click();
		driver.findElement(By.xpath("//*[@id='phonesystem']/select-dropdown/div/div[2]/ul/li[1]")).click();

		driver.findElement(By.xpath("//*[@id='ip']")).sendKeys("127.0.0.0");

		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("tenfold@tenfold.com");
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id='finish-button']")).click();
		
		ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/div[2]/app-review/h1"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[1]/div/div/a")).getText().contains("Tenfold - QA Enginner Test"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/h1")).getText().contains("Review Page"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/h3[1]")).getText().contains("User Looged"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/p[1]")).getText().contains("username:"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/h3[2]")).getText().contains("CRM"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/p[2]")).getText().contains("e-mail:"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/p[2]")).getText().contains("password:"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/h3[3]")).getText().contains("Phone System"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/p[4]")).getText().contains("type:"));
		Assert.assertTrue(driver.findElement(By.xpath("/html/body/app-root/div[2]/app-review/p[4]")).getText().contains("username:"));

	}
		
	@After
	public void tearDown() {
		
		driver.close();
		driver.quit();
	}

}
